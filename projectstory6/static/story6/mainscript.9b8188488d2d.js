var myVar;
function myFunction() {
myVar = setTimeout(showPage, 3000);
}

function showPage() {
document.getElementById("loader").style.display = "none";
document.getElementById("myDiv").style.display = "block";
}

$(document).ready(function(){
    var correct = true;
    $("#change-theme").click(function(){
      if (correct){
        $("body").css("background-color", "black");
        $("#form").css("color", "white");
        $(".par").css("color", "white");
        $("nav").css("background-color",'black');
        $(".navbar-brand").css("color","white");
        $(".nav-color").css("color","white");
        correct = false;
      } else {
        $("body").css("background-color", "white");
        $("#form").css("color", "black");
        $(".par").css("color", "black");
        $("nav").css("background-color",'#42f48c');
        $(".navbar-brand").css("color","black");
        $(".nav-color").css("color","black");
        correct = true;
      }
      });

    $( function() {
      $("#accordion").accordion();
    });

    // $( function() {
    //   $.ajax({
    //     type: "GET",
    //     url: "/read_json/",
    //     dataType: "json",
    //     success: function(result){
    //       var item = result["items"]
    //       var line = ""
    //       for(var i=0;i < item.length; i++){
    //         var res = item[i]
    //         var image = res["volumeInfo"]["imageLinks"]["smallThumbnail"]
    //         var author = res["volumeInfo"]["authors"]
    //         var publisher = res["volumeInfo"]["publisher"]
    //         var date = res["volumeInfo"]["publishedDate"]
    //         line += "<tr><td><img src=" + image + "></td>"
    //         line += "<td>" + author + "</td>"
    //         line += "<td>" + publisher + "</td>"
    //         line += "<td>" + date + "</td>"
    //         line += "<td><button type='button' id='buttonbintang' class='bintang'><img src='/static/staroff.png' height='30px' width='30px'></button></td></tr>"
    //       }
    //       $("#book_list").html(line)
    //     }
    //     });
    // });

    var count = 0;
    $(document).on('click', '#buttonbintang',function(){
      console.log($("#buttonbintang").css('background-color'));
      if($(this).css('background-color') == 'rgb(255, 255, 0)'){
        count -= 1;
        console.log("he");
        $(this).css('background-color','rgb(255, 255, 255)');
      }
      else{
        $(this).css('background-color','yellow');
        console.log("bct")
        count++;
      }
      $("#suka").html(count);
    });

    var toPrint = "";
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $('#id_email').change(function(){
        $.ajax({
            headers: {"X-CSRFToken": csrftoken,},
            type:"POST",
            url: "/check/",
            data: $('#id_email').serialize(),
            dataType: 'json',
            success: function(res) {
                console.log(res);
                if (res.is_exist == true ) {
                    $("input#submit").hide();
                    toPrint += '<div class="alert alert-danger" role="alert" style="text-align: center">The button is missing! Use another email and then it will be back</div>'
                    $('#alert').html(toPrint)
                }
                else {
                    $("input#submit").show(); 
                }
            }
        });
    });

    $("#form").submit(function(e) {
        $.ajax({
            type: "POST",
            url: "/save_register/",
            data: $('form').serialize(),
            success: function(results) {  
              var toPrint = ''
              if(results.status == 'valid') {
                toPrint += '<div class="alert alert-success" role="alert" style="text-align: center">Thank you for subscribing. You are ready to go!</div>'
                $('#alert').html(toPrint)
                $('#subs_table').show()
              }                
            }    
        });
        e.preventDefault();
    });

    $.ajax({
          type: "GET",
          url: "/userdata/",
          dataType:"json",
          //munculin data user
          success: function(res) {
            var new_user = "";
            for(var i =0;i<res.length;i++){
              var name = res[i].name;
              var email = res[i].email;
              new_user += "<tr id='delete-" + i + "><td>" + name + "</td>"
              new_user += "<td>" + email + "</td>"
              new_user += "<td><button type='button' class='btn btn-light' onClick=unsub('"+ email + "',"+ i +")>Unsubscribe</button></td></tr>"
            }
            $("#table_body").append(new_user); // isi table dengan nama, email user, serta tombol unsubscribe
          }
      });
});

function unsub(email,i) {
  $.ajax({
    type:"GET",
    url: "userdelete/?email=" + email,
    dataType: "json",
    success: function() {
      $("#delete-" + i).remove();
    }
  });
}
