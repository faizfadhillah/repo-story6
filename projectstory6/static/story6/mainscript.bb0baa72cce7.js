var myVar;
function myFunction() {
myVar = setTimeout(showPage, 3000);
}

function showPage() {
document.getElementById("loader").style.display = "none";
document.getElementById("myDiv").style.display = "block";
}

$(document).ready(function(){
    var correct = true;
    $("#change-theme").click(function(){
      if (correct){
        $("body").css("background-color", "black");
        $("#form").css("color", "white");
        $(".par").css("color", "white");
        $("nav").css("background-color",'black');
        $(".navbar-brand").css("color","white");
        $(".nav-color").css("color","white");
        correct = false;
      } else {
        $("body").css("background-color", "white");
        $("#form").css("color", "black");
        $(".par").css("color", "black");
        $("nav").css("background-color",'#42f48c');
        $(".navbar-brand").css("color","black");
        $(".nav-color").css("color","black");
        correct = true;
      }
      });

    $( function() {
      $("#accordion").accordion();
    });

    $( function() {
      $.ajax({
        type: "GET",
        url: "/read_json/",
        dataType: "json",
        success: function(result){
          var item = result["items"]
          var line = ""
          for(var i=0;i < item.length; i++){
            var res = item[i]
            var image = res["volumeInfo"]["imageLinks"]["smallThumbnail"]
            var author = res["volumeInfo"]["authors"]
            var publisher = res["volumeInfo"]["publisher"]
            var date = res["volumeInfo"]["publishedDate"]
            line += "<tr><td><img src=" + image + "></td>"
            line += "<td>" + author + "</td>"
            line += "<td>" + publisher + "</td>"
            line += "<td>" + date + "</td>"
            line += "<td><button type='button' id='count' class='bintang'><img src='/static/staroff.png' height='30px' width='30px'></button></td></tr>"
          }
          $("#book_list").html(line)
        }
        });
    });


    var count = 0;
    $(document).on('click','.bintang',function(){
      if($(this).hasClass('clicked')) {
        count -= 1;
        $(this).css('background-color','grey');
        $(this).removeClass('clicked');
      }
      else{
        $(this).addClass('clicked');
        $(this).css('background-color','yellow');
        count += 1;
      }
      $("#count").html(count);
    });
});
