var myVar;
function myFunction() {
myVar = setTimeout(showPage, 3000);
}

function showPage() {
document.getElementById("loader").style.display = "none";
document.getElementById("myDiv").style.display = "block";
}

$(document).ready(function(){
    var correct = true;
    $("#change-theme").click(function(){
      if (correct){
        $("body").css("background-color", "black");
        $("#form").css("color", "white");
        $(".par").css("color", "white");
        $("nav").css("background-color",'black');
        $(".navbar-brand").css("color","white");
        $(".nav-color").css("color","white");
        correct = false;
      } else {
        $("body").css("background-color", "white");
        $("#form").css("color", "black");
        $(".par").css("color", "black");
        $("nav").css("background-color",'#42f48c');
        $(".navbar-brand").css("color","black");
        $(".nav-color").css("color","black");
        correct = true;
      }
      });

    $( function() {
      $( "#accordion" ).accordion();
    } );


});
