from django.contrib.auth import logout
from django.http import HttpResponseRedirect

def logout_view(request):
	try:
		del request.session['username']
	except:
		pass
	logout(request)
	return HttpResponseRedirect('/books')
