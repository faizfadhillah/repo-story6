"""projectstory6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import re_path
from django.contrib.auth import views
from story6.views import *
# from .views import logout_view
import story6.urls as story6

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^index/', index, name="index"),
    re_path(r'^profile/', profile, name="profile"),
    # re_path(r'^books/', books, name="books"),
    re_path(r'^login/', views.LoginView, name='login'),
    re_path(r'^logout/', logout_view, name='logout'),
    re_path(r'^auth/', include('social_django.urls', namespace='social')),
    re_path(r'^', include (story6))
]
