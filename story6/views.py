from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.db import IntegrityError
from django.contrib.auth import authenticate, login, logout
from .forms import *
from .models import *
import json
import requests

# Create your views here.

response = {}

def index(request):
    response['savestatus'] = statusforms
    table = statusmodels.objects.all()
    response['table'] = table
    html = 'index.html'

    if(request.method == 'POST'):
        response['from_who'] = request.POST['from_who']
        response['statuses'] = request.POST['statuses']
        writestat = statusmodels(from_who=response['from_who'],statuses=response['statuses'])
        writestat.save()
        html = 'index.html'
        return render(request, html, response)
    else:
        return render(request, html, response)

def profile(request):
    html = 'profile.html'
    return render(request,html)

def read_json(request):
    read_books = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    parse_read = read_books.json()
    return JsonResponse(parse_read)

def search(request):
    response = {}
    response['form'] = SearchForm()
    response['data'] = None
    
    if request.method == 'POST':
        search = request.POST.get('search')
        URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
        get_json = requests.get(URL).json()
        json_dict = json.dumps(get_json)
        response['data'] = json_dict
        return render(request, "books.html", response)
    else:
        return render(request, "books.html", response)

def books_counter(request):
    if request.user.is_authenticated:
        request.session['user'] = request.user.username
        request.session['emai'] = request.user.email
        request.session.get('counter',0)
        for key, value in request.session.items():
            print('{} = {}'.format(key,value))
    return render(request,"books.html")

def logout_view(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/books/')

def increase_counter(request):
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'])

def decrease_counter(request):
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'])

def register(request):
    form = RegistrationForm(request.POST)
    html = 'registrationForm.html'
    return render(request,html,{'form':form})

def check(request):
    email = request.POST['email']
    data = RegistrationModel.objects.filter(email=email)
    if data.exists():
        return JsonResponse({'is_exist':True})
    else:
        return JsonResponse({'is_exist':False})

def save_register(request):
    # html = 'registrationForm.html'
    response['form'] = RegistrationForm()
    form = RegistrationForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        status = 'valid'
        try:
            write_register = RegistrationModel(name=response['name'],email=response['email'],password=response['password'])
            write_register.save()
            return JsonResponse({'status':status})

        except IntegrityError as e:
            status = 'invalid'
            return JsonResponse({'status':status})
    else:
        return render(request,'registrationForm.html',response)

def userdata(request):
    all_obj = RegistrationModel.objects.all().values()
    user_list = list(all_obj)
    return JsonResponse(user_list, safe= False)

def userdelete(request):
    email = request.GET['email']    
    RegistrationModel.objects.get(email=email).delete()
    return JsonResponse({})

# def login(request):
#     username = request.POST['username']
#     password = request.POST['password']
#     user = authenticate(request, username=username, password=password)
#     if user is not None:
#         return HttpResponseRedirect('/books')
#     else:
#         return HttpResponseRedirect('/')


