from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AdminTestCase(TestCase):
    def setUp(self):
        # setUp is where you instantiate the selenium webdriver and loads the browser.
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(AdminTestCase, self).setUp()

    def tearDown(self):
        # Call tearDown to close the web browser
        self.selenium.quit()
        super(AdminTestCase, self).tearDown()

    def test_create_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        #selenium.get('http://localhost:8000/')
        selenium.get('https://story6-faizfadhillah.herokuapp.com/')
        from_who = selenium.find_element_by_name('from_who')
        statuses = selenium.find_element_by_name('statuses')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        from_who.send_keys('Kak Pewe')
        statuses.send_keys('Coba Coba')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn("Coba Coba",selenium.page_source)

    def test_header_background_color(self):
        selenium = self.selenium
        #selenium.get('http://localhost:8000/')
        selenium.get('https://story6-faizfadhillah.herokuapp.com/')
        navbar = selenium.find_element_by_tag_name('nav')
        self.assertEqual('rgba(66, 244, 140, 1)',navbar.value_of_css_property('background-color'))

    # def test_header_display(self):
    #     selenium = self.selenium
    #     #selenium.get('http://localhost:8000/')
    #     selenium.get('https://story6-faizfadhillah.herokuapp.com/')
    #     header = selenium.find_element_by_tag_name('h1')
    #     self.assertEqual('center',header.value_of_css_property('text-align'))
    #
    # def test_header_text(self):
    #     selenium = self.selenium
    #     #selenium.get('http://localhost:8000/')
    #     selenium.get('https://story6-faizfadhillah.herokuapp.com/')
    #     hello = selenium.find_element_by_css_selector("h1.text-center").text
    #     self.assertEqual('Hello, Apa kabar?',hello)

    # def test_navbar_text(self):
    #     selenium = self.selenium
    #     #selenium.get('http://localhost:8000/')
    #     selenium.get('https://story6-faizfadhillah.herokuapp.com/')
    #     navbar = selenium.find_element_by_css_selector("a.navbar-brand").text
    #     self.assertEqual('Faiz Fadhillah',navbar)

# Create your tests here.
class TestTDD(TestCase):
    #checking if the link is directing to story6
    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    #checking if the link uses my index function on views.py
    def test_story_6_using_index_func(self):
        found = resolve('/index/')
        self.assertEqual(found.func, index)

    def test_story_6_using_profile_func(self):
        found_main = resolve('/profile/')
        self.assertEqual(found_main.func, profile)

    def test_story_6_using_index_template(self):
        response = Client().get('/index/')
        self.assertTemplateUsed(response, 'index.html')

    def test_profile_is_completed(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('-Faiz Fadhillah Soemawilaga-', html_response)
        self.assertIn('1706027515', html_response)

    #checking if the content has "hello apa kabar" in it
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    #checking if people can put the status in
    def test_model_can_create_new_status(self):
        # Creating new status
        new_status = statusmodels.objects.create(from_who= 'mahasiswa pusing', statuses='lagi dikerjar deadlines')

        # Retrieving status that has been made
        counting_all_status = statusmodels.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_story_6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/index/', {'from_who': test, 'statuses': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/index/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story_8_using_books_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_story_10_model_can_create_new_user(self):
        # Creating new user
        new_user = RegistrationModel.objects.create(name= 'test', password='test', email='test@test.com')

        # Retrieving status that has been made
        count_all_user = RegistrationModel.objects.all().count()
        self.assertEqual(count_all_user, 1)

    def test_stroy_10_check_email_already_exist_view_get_return_200(self):
        RegistrationModel.objects.create(name='test', email="test@test.com", password="testtest")
        response = Client().post('/check/', data={"email": "test@test.com"})
        self.assertEqual(response.json()['is_exist'], True)

    def test_story_10_check_email_hasnt_been_used(self):
        response = Client().post('/check/', data={"email": "differenttest@gmail.com"})
        self.assertEqual(response.json()['is_exist'], False)

