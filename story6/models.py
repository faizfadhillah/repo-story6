from django.db import models
from django.utils import timezone

#Create your models here.

class statusmodels(models.Model):
    from_who = models.TextField(max_length=300)
    statuses = models.TextField(max_length=300)
    date_filled = models.DateField(default=timezone.now)

class RegistrationModel(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=25)

# class UserManager(models.Manager):
#     def create_user(self, username, email):
#         return self.model._default_manager.create(username=username)

# class CustomUser(models.Model):
#     username = models.CharField(max_length=30)
#     objects = UserManager
    
