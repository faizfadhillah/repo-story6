from django.contrib import admin
from .models import statusmodels, RegistrationModel

# Register your models here.
admin.site.register(statusmodels)
admin.site.register(RegistrationModel)