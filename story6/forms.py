from django import forms
from .models import statusmodels, RegistrationModel

class statusforms(forms.Form):
    error_messages = {
        'required': 'Mohon diisi',
    }
    from_who = forms.CharField(label='Name', required=True, widget=forms.TextInput(attrs={'type': 'text', 'class':'form-control'}))
    statuses = forms.CharField(label='Status', required=True, widget=forms.TextInput(attrs={'type': 'text', 'class':'form-control'}))

class SearchForm(forms.Form):
    search_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Search book',
    }
    title = forms.CharField(max_length = 300, label='', widget=forms.TextInput(attrs=search_attrs))

class RegistrationForm(forms.Form):
    name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'type': 'text', 'class':'form-control'}))
    email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'type': 'text', 'class':'form-control'}))
    password = forms.CharField(label='Password',widget=forms.TextInput(attrs={'type': 'password', 'class':'form-control'}))
