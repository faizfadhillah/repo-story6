from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .views import *


urlpatterns = [
    url(r'^index/', index, name='index'),
    url(r'^profile/', profile, name='profile'),
    url(r'^books/', search, name='data'),
    url(r'^increase_counter/', increase_counter, name='increase_counter'),
    url(r'^decrease_counter/', decrease_counter, name='decrease_counter'),
    url(r'^logout/', logout_view, name='logout_view'),
    url(r'^read_json/', read_json, name='read_json'),
    url(r'^books_counter/', books_counter, name='books_counter'),
    url(r'^check/', check, name='check'),
    url(r'^save_register/', save_register, name='save_register'),
    url(r'^register/', register, name='register'),
    url(r'^userdata/', userdata, name='userdata'),
    url(r'^userdelete/', userdelete, name='userdelete'),
    url(r'^$', index, name='index'), 

]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
